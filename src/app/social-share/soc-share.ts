export class SocShare {
  constructor(
    public imageUrl: string,
    public linkUrl: string,
    public name: string) { }
}
