import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomePageComponent } from './home-page/home-page.component';
import { AboutPageComponent } from './about-page/about-page.component';
import { ShopPageComponent } from './shop-page/shop-page.component';
import { ContactPageComponent } from './contact-page/contact-page.component';
import { AuthenticationPageComponent } from './authentication-page/authentication-page.component';
import {CartPageComponent} from './cart-page/cart-page.component';

const routes: Routes = [

  { path: '', pathMatch: 'full', redirectTo: '/home' },
  { path: 'home', component: HomePageComponent ,
    data: {animation: 'Home'} },

  { path: 'about', component: AboutPageComponent ,
    data: {animation: 'About'} },

  { path: 'shop', component: ShopPageComponent ,
    data: {animation: 'Shop'} },

  { path: 'contact', component: ContactPageComponent ,
    data: {animation: 'Contact'} },

  { path: 'authentication', component: AuthenticationPageComponent ,
    data: {animation: 'Authentication'} },

  { path: 'cart', component: CartPageComponent ,
    data: {animation: 'Cart'} },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
