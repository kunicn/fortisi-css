import {Component, EventEmitter, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {

  @Output() toggleFormEvent: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  ngOnInit(): void {
  }

  toggleForm() {
    this.toggleFormEvent.emit();
  }

  onSignup() {
    console.log('Somebody Signed up');
  }

}
