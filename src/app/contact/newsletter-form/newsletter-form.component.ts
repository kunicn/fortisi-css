import { Component, OnInit } from '@angular/core';
import {
  FormGroup,
  Validators,
  FormBuilder,
  FormControl
} from '@angular/forms';

@Component({
  selector: 'app-newsletter-form',
  templateUrl: './newsletter-form.component.html',
  styleUrls: ['./newsletter-form.component.css']
})

export class NewsletterFormComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {

    this.registerForm = this.formBuilder.group({
      emailAddress: ['', Validators.required]
    });

    this.
    registerForm.
    valueChanges.
    subscribe(form => {
      sessionStorage.setItem('form', JSON.stringify(form));
    });

    const formValues = sessionStorage.getItem('form');

    if (formValues) {
      this.applyFormValues(this.registerForm, JSON.parse(formValues));
    }

  }

  private applyFormValues(group, formValues) {
    Object.keys(formValues).forEach(key => {
      const formControl = group.controls[key] as FormControl;

      if (formControl instanceof FormGroup) {
        this.applyFormValues(formControl, formValues[key]);
      } else {
        formControl.setValue(formValues[key]);
      }
    });
  }

  destroyFormValues() {
    sessionStorage.removeItem('form');
    alert('Saved form data deleted');
  }



}
